package nl.utwente.di.converterCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**  Tests the Quoeter **/
public class TestCalculator {

    @Test
    public void testBook1() throws Exception {
        Calculator calculator = new Calculator();
        double price = calculator.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of book 1");
    }
}
