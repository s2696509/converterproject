package nl.utwente.di.converterCode;

public class Calculator {

//    private HashMap<String, Double> allBooks = new HashMap<String, Double>();

    public Calculator(){
//        HashMap<String, Double> allBooks = new HashMap<String, Double>();
//
//        allBooks.put("1",10.0);
//        allBooks.put("2",45.0);
//        allBooks.put("3",20.0);
//        allBooks.put("4",35.0);
//        allBooks.put("5",50.0);
//        allBooks.put("others",0.0);
//        this.allBooks = allBooks;
    }

    public double getBookPrice(String isbn){
        double celsiusValue = Double.valueOf(isbn);
        //Formula for converting from celsius to Fahrenheit
        double fahrenheitValue;
        fahrenheitValue = (celsiusValue * 1.8) + 32;
        return fahrenheitValue;
    }

}
